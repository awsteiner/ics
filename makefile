
#----------------------------------------------------------------------

ifdef UTKNA_MAKEFILE

include $(UTKNA_MAKEFILE)

# UTK configuration
LIBS = $(UTKNA_O2SCL_LIBS) 
LCXX = $(UTKNA_CXX) 
LCFLAGS = $(UTKNA_O2SCL_INCS) $(UTKNA_CFLAGS)

endif

#----------------------------------------------------------------------

clean:
	rm -f *.o ics

ics.o: ics.cpp
	$(LCXX) $(UTKNA_PYTHON_INCLUDES) -I/usr/local/include -c ics.cpp

ics: ics.o
	$(LCXX) -o ics ics.o $(UTKNA_O2SCL_LIBS) 

