/*
  -------------------------------------------------------------------
  
  Copyright (C) 2022, Andrew W. Steiner
  
  ics is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  ics is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ics. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <ctime>

#include <o2scl/string_conv.h>

using namespace std;

namespace o2scl_ics {
  
  /*
    tm_sec	int	seconds after the minute	0-61*
    tm_min	int	minutes after the hour	0-59
    tm_hour	int	hours since midnight	0-23
    tm_mday	int	day of the month	1-31
    tm_mon	int	months since January	0-11
    tm_year	int	years since 1900	
    tm_wday	int	days since Sunday	0-6
    tm_yday	int	days since January 1	0-365
    tm_isdst	int	Daylight Saving Time flag	
  */

  /// \name Rule frequency constants
  //@{
  static const int rule_empty=0;
  static const int rule_yearly=1;
  static const int rule_weekly=2;
  static const int rule_daily=3;
  //@}

  /// Days of the week
  static const vector<std::string> weekdays={
    "Sunday","Monday","Tuesday","Wednesday","Thursday",
    "Friday","Saturday"};
  
  /// The tm_t object 
  typedef struct tm tm_t;

  /** \brief Recurrence rule object
   */
  class rule {
    
  public:

    /// Frequency
    int freq;

    /// Ending time
    tm_t tm_until;

    /// Clear the object
    void clear() {
      freq=rule_empty;
      return;
    }

    /// By day specification (not yet supported)
    std::string by_day;
    
    /// By month (not yet supported)
    int by_month;
  };
  
  /** \brief Slightly different interface for asctime
      
      Works with a reference instead of a pointer, returns a string
      instead of a char *, and removes the trailing line feed, if
      present.
  */
  std::string asctime_cpp(const tm_t &t) {
    char *cp=asctime(&t);
    std::string s=cp;
    if (s[s.length()-1]==10) {
      s=s.substr(0,s.length()-1);
    }
    return s;
  }

  /** \brief A calendar object
   */
  class calendar {
  
  public:

    calendar() {
    }
    
    /** \brief The header, not including the "BEGIN:VCALENDAR" and
        "END:VCALENDAR" lines 
    */
    vector<string> header;
    
    /// The event object
    typedef multimap<string,string,less<string>> event_t;

    /// The event field iterator type
    typedef event_t::const_iterator field_it;
    
    /// The events in the calendar
    vector<event_t> events;

    // The calendar's timezone
    std::string cal_tz;
  
    // Offset from GMT
    int gmt_offset;

    /// Clear the object
    void clear() {
      header.clear();
      events.clear();
      gmt_offset=0;
      return;
    }

    /** \brief Output calendar as a .ics file

        \note I force DOS line endings for compatibility.
     */
    void output(std::ostream &outs) const {

      outs << "BEGIN:VCALENDAR\r" << endl;
      for(size_t i=0;i<header.size();i++) {
        outs << header[i] << '\r' << endl;
      }
      for (size_t i=0;i<events.size();i++) {
        outs << "BEGIN:VEVENT" << '\r' << endl;
        for (field_it it=events[i].begin();it!=events[i].end();it++) {
          if (it->first==((string)"DESCRIPTION") ||
              it->first==((string)"DTSTART") ||
              it->first==((string)"DTEND") ||
              it->first==((string)"RECURRENCE-ID") ||
              it->first==((string)"RRULE") ||
              it->first==((string)"ATTENDEE") ||
              it->first==((string)"X-ALT-DESC") ||
              it->first==((string)"TRIGGER") ||
              it->first==((string)"ORGANIZER")) {
            std::string stmp=it->first+";"+it->second;
            if (stmp.length()>75) {
              outs << stmp.substr(0,75) << '\r' << endl;
              stmp=stmp.substr(75,stmp.length()-75);
              bool done=false;
              while (done==false) {
                outs << " " << stmp.substr(0,74) << '\r' << endl;
                if (stmp.length()>74) {
                  stmp=stmp.substr(74,stmp.length()-74);
                } else {
                  done=true;
                }
              }
            } else {
              outs << stmp << '\r' << endl;
            }
          } else {
            outs << it->first << ":" << it->second << '\r' << endl;
          }
        }
        outs << "END:VEVENT" << '\r' << endl;
      }
      outs << "END:VCALENDAR" << '\r' << endl;
      
      return;
    }
    
  };

  /** \brief Parse and manipulate calendar objects
   */
  class ics_parser {

  public:

    /// Convenience typedef for an event
    typedef calendar::event_t event_t;

    /// The event field iterator type
    typedef event_t::const_iterator field_it;
    
    /** \brief Test if events \c e1 and \c e2 overlap, when \c e1
        starts before \c e2 starts
    */
    bool events_overlap(calendar &c, const event_t &e1, const event_t &e2,
                        int verbose=1) {
      bool all_day;
      
      string str_dtend_e1=e1.find("DTEND")->second;
      tm_t tm_dtend_e1=str_to_date(str_dtend_e1,c.cal_tz,c.gmt_offset,
                                     all_day);
      time_t t_dtend_e1=mktime(&tm_dtend_e1);

      string str_dtstart_e2=e2.find("DTSTART")->second;
      tm_t tm_dtstart_e2=str_to_date(str_dtstart_e2,c.cal_tz,c.gmt_offset,
                                     all_day);
      time_t t_dtstart_e2=mktime(&tm_dtstart_e2);

      bool test=(t_dtstart_e2<t_dtend_e1);

      if (verbose>1) {
        cout << "Function events_overlap():" << endl;
        cout << "End of event 1  : " << tm_dtend_e1.tm_hour << " "
             << tm_dtend_e1.tm_min << " " << tm_dtend_e1.tm_sec << endl;
        cout << "Start of event 2: " << tm_dtstart_e2.tm_hour << " "
             << tm_dtstart_e2.tm_min << " " << tm_dtstart_e2.tm_sec << endl;
        cout << "Overlap: " << test << endl;
      }

      return test;
    }
    
    /** \brief Convert a struct tm object to an ICS string
     */
    std::string date_to_str(const tm_t &t) {
      
      std::string s=o2scl::itos(t.tm_year+1900);
      if (t.tm_mon<9) s+="0";
      s+=o2scl::itos(t.tm_mon+1);
      if (t.tm_mday<10) s+="0";
      s+=o2scl::itos(t.tm_mday);
      s+="T";
      if (t.tm_hour<10) s+="0";
      s+=o2scl::itos(t.tm_hour);
      if (t.tm_min<10) s+="0";
      s+=o2scl::itos(t.tm_min);
      if (t.tm_sec<10) s+="0";
      s+=o2scl::itos(t.tm_sec);
      
      return s;
    }

    /** \brief Convert a struct tm object to an ICS string in GMT
     */
    std::string date_to_str_gmt(calendar &c, const tm_t &t) {

      // If we need to add an offset, it's easier to convert to a time_t
      // object, add the correct number of seconds, then convert back
      tm_t tt=t;
      time_t tx=mktime(&tt);
      tx-=c.gmt_offset*3600;
      tm_t t2;
      t2=*localtime(&tx);
      
      std::string s=o2scl::itos(t2.tm_year+1900);
      if (t2.tm_mon<9) s+="0";
      s+=o2scl::itos(t2.tm_mon+1);
      if (t2.tm_mday<10) s+="0";
      s+=o2scl::itos(t2.tm_mday);
      s+="T";
      if (t2.tm_hour<10) s+="0";
      s+=o2scl::itos(t2.tm_hour);
      if (t2.tm_min<10) s+="0";
      s+=o2scl::itos(t2.tm_min);
      if (t2.tm_sec<10) s+="0";
      s+=o2scl::itos(t2.tm_sec);
      s+="Z";
      
      return s;
    }

    /** \brief Convert the date or date/time object from the ics file
        into a struct tm object
    */
    tm_t str_to_date(std::string s, string cal_tz,
                     int gmt_offset, bool &all_day, int verbose=0) {

      all_day=false;
      int correction=0;
      
      if (s.length()>4 && s.substr(0,4)==((string)"TZID")) {
        
        if (verbose>0) {
          cout << "s: " << s << endl;
        }

        // Remove the timezone prefix, and check that it matches
        // the calendary timezone.
        size_t colon=s.find(':');
        if (colon==std::string::npos) {
          O2SCL_ERR("TZID given but no colon found.",
                    o2scl::exc_eunimpl);
        }
        string loc_tz=s.substr(5,colon-5);
        s=s.substr(colon+1,s.length()-colon-1);
        if (verbose>0) {
          cout << loc_tz << " " << s << endl;
        }
        if (loc_tz==((string)"America/Los_Angeles") &&
            cal_tz==((string)"America/New_York")) {
          correction=3;
        } else if (loc_tz!=cal_tz) {
          std::cout << "loc_tz,cal_tz: " << loc_tz << " , "
                    << cal_tz << endl;
          O2SCL_ERR("Time zone mismatch.",o2scl::exc_eunimpl);
        }
    
      } else if (s.length()>11 && s.substr(0,11)=="VALUE=DATE:") {

        s=s.substr(11,8);
        if (s.length()<8) {
          O2SCL_ERR("VALUE=DATE: prefix but no date given.",
                    o2scl::exc_efailed);
        }
        int year_temp=o2scl::stoi(s.substr(0,4));
        int month_temp=o2scl::stoi(s.substr(4,2));
        int day_temp=o2scl::stoi(s.substr(6,2));
    
        std::string s2=o2scl::itos(year_temp)+" "+
          o2scl::itos(month_temp)+" "+o2scl::itos(day_temp)+" "+
          "000000";
    
        if (verbose>0) {
          cout << "s2: x" << s2 << "x" << endl;
        }

        tm_t t;
        strptime(s2.c_str(),"%Y %m %d %H%M%S",&t);

        if (verbose>0) {
          cout << "sec: " << t.tm_sec << " min: " << t.tm_min << " "
               << "hr: " << t.tm_hour << " mday: " << t.tm_mday << " "
               << "mon: " << t.tm_mon << " year: " << t.tm_year << " "
               << "wday: " << t.tm_wday << " yday: " << t.tm_yday << " "
               << "isdst: " << t.tm_isdst << endl;
        }
    
        all_day=true;
        return t;
      }
  
      // Remove trailing ctrl-M, if present
      if (s[s.length()-1]==13) {
        s=s.substr(0,s.length()-1);
      }

      // Determine if this is a GMT time or a local time
      bool gmt=false;
      if (s[s.length()-1]=='Z') {
        gmt=true;
        s=s.substr(0,s.length()-1);
      }

      // Check to make sure that the string has the expected format
      size_t pos=s.find('T');
      if (pos==string::npos) {
        O2SCL_ERR("No 'T' in date string in str_to_date().",
                  o2scl::exc_einval);
      }

      // Convert to a time object
      tm_t t;

      // Reformat the string into a string which can be parsed by strptime()
      string first=s.substr(0,pos);
      string second=s.substr(pos+1,s.length()-pos-1);

      if (first.length()<8) {
        O2SCL_ERR("Cannot find date.",o2scl::exc_efailed);
      }
      int year_temp=o2scl::stoi(first.substr(0,4));
      int month_temp=o2scl::stoi(first.substr(4,2));
      int day_temp=o2scl::stoi(first.substr(6,2));

      std::string s2=o2scl::itos(year_temp)+" "+
        o2scl::itos(month_temp)+" "+o2scl::itos(day_temp)+" "+
        second;

      if (verbose>0) {
        cout << "s2: " << s2 << endl;
      }
  
      strptime(s2.c_str(),"%Y %m %d %H%M%S",&t);

      if (verbose>0) {
        cout << "before changes sec: " << t.tm_sec
             << " min: " << t.tm_min << " "
             << "hr: " << t.tm_hour << " mday: " << t.tm_mday << " "
             << "mon: " << t.tm_mon << " year: " << t.tm_year << " "
             << "wday: " << t.tm_wday << " yday: " << t.tm_yday << " "
             << "isdst: " << t.tm_isdst << endl;
      }

      if (correction!=0) {

        // If we need to add a correction, it's easier to convert to a time_t
        // object and just add the correct number of seconds
        time_t tx=mktime(&t);
        tx+=correction*3600;
        t=*localtime(&tx);

        if (verbose>0) {
          cout << "correction: " << correction << endl;
          cout << "after correction sec: " << t.tm_sec
               << " min: " << t.tm_min << " "
               << "hr: " << t.tm_hour << " mday: " << t.tm_mday << " "
               << "mon: " << t.tm_mon << " year: " << t.tm_year << " "
               << "wday: " << t.tm_wday << " yday: " << t.tm_yday << " "
               << "isdst: " << t.tm_isdst << endl;
        }
        
      }
      
      if (gmt) {
        
        // If we need to add an offset, it's easier to convert to a time_t
        // object and just add the correct number of seconds
        time_t tx=mktime(&t);
        tx+=gmt_offset*3600;
        t=*localtime(&tx);

        if (verbose>0) {
          cout << "gmt_offset: " << gmt_offset << endl;
          cout << "after gmt_offset sec: " << t.tm_sec
               << " min: " << t.tm_min << " "
               << "hr: " << t.tm_hour << " mday: " << t.tm_mday << " "
               << "mon: " << t.tm_mon << " year: " << t.tm_year << " "
               << "wday: " << t.tm_wday << " yday: " << t.tm_yday << " "
               << "isdst: " << t.tm_isdst << endl;
        }
    
      }

      return t;
    }

    /** \brief Insert event \c ordered by start time
     */
    void insert_ordered(calendar &c, event_t &e, int verbose=2) {

      if (e.find("X-MOZ-FAKED-MASTER")!=e.end()) {
        cout << "Skipping event with entry X-MOZ-FAKED-MASTER." << endl;
        return;
      }
      
      bool all_day;
      string str_dtstart_e=e.find("DTSTART")->second;
      tm_t tm_e=str_to_date(str_dtstart_e,c.cal_tz,c.gmt_offset,all_day);
      time_t t_e=mktime(&tm_e);

      if (verbose>1) {
        cout << "Function insert_ordered(), events.size(): "
             << c.events.size() << endl;
        cout << "  HMS, summary of event: " << tm_e.tm_hour << " "
             << tm_e.tm_min << " " << tm_e.tm_sec << " ";
        if (e.find("SUMMARY")!=e.end()) {
          cout << e.find("SUMMARY")->second << endl;
        } else if (e.find("UID")!=e.end()) {
          cout << "UID: " << e.find("UID")->second << endl;
        } else {
          cout << "(no summary)" << endl;
        }
      }

      if (c.events.size()==0) {
        c.events.push_back(e);
        return;
      }

      bool done=false;
      for(size_t i=0;done==false && i<c.events.size();i++) {
        string value=c.events[i].find("DTSTART")->second;
        tm_t tm=str_to_date(value,c.cal_tz,c.gmt_offset,all_day);
        time_t t=mktime(&tm);
        if (verbose>1) {
          cout << "event: " << i << " hour: " << tm.tm_hour << " ";
          if (c.events[i].find("SUMMARY")!=c.events[i].end()) {
            cout << c.events[i].find("SUMMARY")->second << endl;
          } else if (c.events[i].find("UID")!=c.events[i].end()) {
            cout << "UID: " << c.events[i].find("UID")->second << endl;
          } else {
            cout << "(no summary)" << endl;
          }
        }
        if (i==0 && t_e<=t) {
          if (verbose>1) {
            cout << "Before beginning." << endl;
          }
          c.events.insert(c.events.begin(),e);
          done=true;
        } else if (i==c.events.size()-1 && t_e>=t) {
          if (verbose>1) {
            cout << "After end" << endl;
          }
          c.events.insert(c.events.begin()+i+1,e);
          done=true;
        } else if (i<c.events.size()-1) {
          string value2=c.events[i+1].find("DTSTART")->second;
          tm_t tm2=str_to_date(value2,c.cal_tz,c.gmt_offset,all_day);
          time_t t2=mktime(&tm2);
          if (verbose>1) {
            cout << "next event: " << i+1 << " hour: " << tm2.tm_hour << endl;
          }
          if (t<t_e && t_e<t2) {
            if (verbose>1) {
              cout << "In between." << endl;
            }
            c.events.insert(c.events.begin()+i+1,e);
            done=true;
          }
        }
      }

      //for(size_t i=0;i<c.events.size();i++) {
      //cout << c.events[i].find("SUMMARY")->second << endl;
      //}
      
      //char ch;
      //cin >> ch;

      if (done==false) {
        O2SCL_ERR("Function insert_ordered() failed.",o2scl::exc_esanity);
      }
      
      return;
    }
    
    /** \brief Parse the rule stored in \c value, determine the frequency,
        and end time
    */
    int parse_rule(calendar &c, string value, rule &r, int verbose=0) {
  
      string str_until;
      if (value.length()>18 &&
          value.substr(0,18)=="FREQ=WEEKLY;UNTIL=") {
        str_until=value.substr(18,value.length()-18);
        r.freq=rule_weekly;
      } else if (value.length()>12 &&
                 value.substr(0,12)=="FREQ=YEARLY;") {
        str_until=value.substr(12,value.length()-12);
        r.freq=rule_yearly;
        O2SCL_ERR("RRULE not understood.",o2scl::exc_eunimpl);
      } else if (value.length()>17 &&
                 value.substr(0,17)=="FREQ=DAILY;UNTIL=") {
        str_until=value.substr(17,value.length()-17);
        r.freq=rule_daily;
      } else {
        cout << "value: " << value << endl;
        O2SCL_ERR2("Rule not understood ",
                  "in parse_rule().",o2scl::exc_eunimpl);
      }
      
      if (r.freq==rule_weekly || r.freq==rule_daily) {
        bool all_day;
        r.tm_until=str_to_date(str_until,c.cal_tz,c.gmt_offset,all_day);
        string sxx=asctime_cpp(r.tm_until);
        if (verbose>0) {
          if (r.freq==rule_weekly) {
            cout << "  Weekly until " << sxx << endl;
          } else {
            cout << "  Daily until " << sxx << endl;
          }
        }
      }
  
      return 0;
    }

    /** \brief Parse schedule in \c filename and place in 
        calendar \c c
     */
    int parse(std::string filename, calendar &c, int verbose=2) {
    
      // A temporary string for parsing
      string stmp;

      // Begin parsing
      ifstream fin(filename);

      // Header section
      bool header_done=false;
      while (header_done==false) {
        getline(fin,stmp);
        if (stmp.substr(0,12)==((string)"BEGIN:VEVENT")) {
          if (verbose>1) {
            cout << "Header done." << endl;
          }
          header_done=true;
        } else {
          if (stmp.substr(0,15)!=((string)"BEGIN:VCALENDAR")) {
            if (stmp[stmp.length()-1]==13) {
              stmp=stmp.substr(0,stmp.length()-1);
            }
            if (verbose>1) {
              cout << "Header line: " << stmp << endl;
            }
            c.header.push_back(stmp);
            if (stmp.substr(0,5)==((string)"TZID:")) {
              c.cal_tz=stmp.substr(5,stmp.length()-5);
              // Remove trailing ctrl-m
              if (c.cal_tz[c.cal_tz.length()-1]==13) {
                c.cal_tz=c.cal_tz.substr(0,c.cal_tz.length()-1);
              }
              if (c.cal_tz==((string)"America/New_York")) {
                c.gmt_offset=-5;
              }
            }
          }
        }
      }

      if (verbose>0) {
        std::cout << "Detected timezone " << c.cal_tz << " and offset "
                  << c.gmt_offset << std::endl;
      }

      // ----------------------------------------------------------------
      // Event section
  
      bool cal_done=false;

      // The current event
      multimap<string,string,less<string>> event;

      // A pointer to the last string, used for line continuations
      string *last=0;
  
      while (cal_done==false) {
        getline(fin,stmp);
        if (stmp.substr(0,10)==((string)"END:VEVENT")) {
          if (verbose>1) {
            cout << "Event end." << endl;
            cout << "Adding event." << endl;
          }
          c.events.push_back(event);
          if (verbose>1) {
            cout << "Clearing event object." << endl;
          }
          event.clear();
          getline(fin,stmp);
          if (stmp.substr(0,13)==((string)"END:VCALENDAR")) {
            if (verbose>1) {
              cout << "Calendar end." << endl;
            }
            cal_done=true;
          }
        } else {
          if (stmp[0]==' ' && last!=0) {
            string stmp2=stmp.substr(1,stmp.length()-1);
            if (verbose>1 && last->length()<500) {
              cout << "Continuation: " << stmp2 << endl;
            }
            *last+=stmp2;
          } else {
            size_t colon=0;
            for(size_t j=0;j<stmp.length() && colon==0;j++) {
              if (!isalnum(stmp[j]) && (stmp[j]==';' || stmp[j]==':')) {
                colon=j;
              }
            }
            string field=stmp.substr(0,colon);
            string value=stmp.substr(colon+1,stmp.length()-colon-1);
            if (value.length()>0 && value[value.length()-1]==13) {
              value=value.substr(0,value.length()-1);
            }
            if (field==((string)"LAST-MODIFIED") ||
                field==((string)"CREATED") ||
                field==((string)"DTSTART") ||
                field==((string)"DTEND") ||
                field==((string)"DTSTAMP")) {
              bool all_day;
              tm_t tm=str_to_date(value,c.cal_tz,c.gmt_offset,all_day);
              string sxx=asctime_cpp(tm);
              if (verbose>1) {
                cout << "Inserting field: " << field << " and value: "
                     << value << " " << sxx;
                if (all_day) {
                  cout << " all day";
                }
                cout << endl;
              }
            } else if (verbose>1) {
              cout << "Inserting field: " << field << " and value: "
                   << value << endl;
            }
            if (field==((string)"RRULE") &&
                value.substr(0,5)==((string)"FREQ=")) {
              rule r;
              parse_rule(c,value,r);
            }
            event.insert(make_pair(field,value));
            last=&(event.find(field)->second);
          }
        }
        if (verbose>2) {
          char ch;
          cin >> ch;
        }
      }

      if (verbose>1) {
        cout << endl;
      }
      if (verbose>0) {
        cout << "Read " << c.events.size() << " events from "
             << filename << " ." << endl;
      }
      if (verbose>1) {
        cout << endl;
      }

      return 0;
    }

    /** \brief Create a list of events for next week
     */
    int make_week(calendar &c, vector<calendar> &events_nw,
                  int week_offset=0, int verbose=2) {
      
      // Set up the calendar list
      events_nw.clear();
      events_nw.resize(7);

      // Copy the original calendar timezone and offset
      for(size_t j=0;j<7;j++) {
        events_nw[j].cal_tz=c.cal_tz;
        events_nw[j].gmt_offset=c.gmt_offset;
      }
      
      // Current time
      time_t now;
      time(&now);
      tm_t tnow=*localtime(&now);
      if (verbose>1) {
        cout << "Now: " << asctime_cpp(tnow) << endl;
      }

      // Set to Sunday
      if (tnow.tm_wday!=0) {
        now-=3600*24*tnow.tm_wday;
        tnow=*localtime(&now);
      }
      
      // Perform week ofset
      now+=week_offset*7*24*3600;
      tnow=*localtime(&now);

      // Backtrack to midnight
      time_t sun_mid=now;
      sun_mid-=60*tnow.tm_min;
      sun_mid-=tnow.tm_sec;
      sun_mid-=3600*(tnow.tm_hour);
      tm_t tsun_mid=*localtime(&sun_mid);
      if (verbose>1) {
        cout << "Sunday midnight: " << asctime_cpp(tsun_mid) << endl;
        cout << endl;
      }

      // Go through and find events happening next week
      bool found_an_all_day=false;
      
      for(size_t i=0;i<c.events.size();i++) {

        // Find the start and end times of this event
        string start=c.events[i].find("DTSTART")->second;
        string end=c.events[i].find("DTEND")->second;
        bool all_day_start, all_day_end;
        tm_t tm_start=str_to_date(start,c.cal_tz,c.gmt_offset,all_day_start);
        time_t t_start=mktime(&tm_start);
        tm_t tm_end=str_to_date(end,c.cal_tz,c.gmt_offset,all_day_end);
        time_t t_end=mktime(&tm_end);

        if ((all_day_start && !all_day_end) ||
            (all_day_end && !all_day_start)) {
          O2SCL_ERR2("Partial all-day events not supported ",
                    "in make_week().",o2scl::exc_eunimpl);
        }

        // Compute differences with Sunday at midnight
        double dt_start=difftime(sun_mid,t_start)/(24*3600);
        double dt_end=difftime(sun_mid,t_end)/(24*3600);

        if (all_day_start && all_day_end) {
          
          if (c.events[i].find("RRULE")!=c.events[i].end()) {
            O2SCL_ERR2("All day events with rules not supported ",
                      "in make_week().",o2scl::exc_eunimpl);
          }

          // If it's an all day event, then it might be a multi-day
          // event, so we create a loop of days to add. 
          int ix_begin=-dt_start;
          int ix_end=-dt_end;

          // If it begins before Monday, only start adding it on Monday
          if (ix_begin<1 && ix_end<6) ix_begin=1;

          // Only loop through Friday
          for(int ix=ix_begin;ix<ix_end && ix<=5;ix++) {

            // Add the all day event and ensure that a row for all-day
            // events is included in the calendar
            insert_ordered(events_nw[ix],c.events[i]);
            found_an_all_day=true;
            
          }
          
        } else if (dt_start<0.0 && dt_start>-7.0) {
          
          if (verbose>1) {
            cout << "Event happening next week:" << endl;
            cout << "  Start HMS: "
                 << tm_start.tm_hour << " " << tm_start.tm_min << " "
                 << tm_start.tm_sec << endl;
            cout << "  End HMS: "
                 << tm_end.tm_hour << " " << tm_end.tm_min << " "
                 << tm_end.tm_sec << endl;
          }
          if (c.events[i].find("SUMMARY")!=c.events[i].end() && verbose>1) {
            cout << "  Summary: " << c.events[i].find("SUMMARY")->second
                 << endl;
          }

          // Determine which day to add it to
          int ix=-dt_start;
          if (ix<0 || ix>6) {
            O2SCL_ERR("Indexing error in next_week().",o2scl::exc_esanity);
          }

          // Add the event
          insert_ordered(events_nw[ix],c.events[i]);

        }

        // If the event has a rule, then determine 
        if (c.events[i].find("RRULE")!=c.events[i].end()) {
          
          string value=c.events[i].find("RRULE")->second;
          rule r;
          parse_rule(c,value,r);
          time_t t_until=mktime(&r.tm_until);

          if (t_until>=sun_mid) {
            if (verbose>1) {
              cout << "Rule does not end before next Sunday." << endl;
            }
            if (c.events[i].find("SUMMARY")!=c.events[i].end() && verbose>1) {
              cout << "  Summary: " << c.events[i].find("SUMMARY")->second
                   << endl;
            }
            if (r.freq==rule_weekly) {
              
              bool done=false;

              // Only consider the first 100 weeks for any one event
              for(size_t j=0;j<100 && done==false;j++) {
                
                t_start+=7*24*3600;
                t_end+=7*24*3600;
                
                dt_start=difftime(sun_mid,t_start)/(24*3600);
                dt_end=difftime(sun_mid,t_end)/(24*3600);
                
                tm_t tm_start2=*localtime(&t_start);
                tm_t tm_end2=*localtime(&t_end);

                // Adding a week of seconds doesn't work if the rule
                // goes over daylight savings time, so we adjust by
                // subtracting an hour or adding an hour if necessary.
                
                if (tm_start2.tm_hour-tm_start.tm_hour==1) {
                  cout << "Adjusting." << endl;
                  
                  t_start-=3600;
                  t_end-=3600;
                  
                  dt_start=difftime(sun_mid,t_start)/(24*3600);
                  dt_end=difftime(sun_mid,t_end)/(24*3600);
                  
                  tm_start2=*localtime(&t_start);
                  tm_end2=*localtime(&t_end);
                  
                } else if (tm_start2.tm_hour-tm_start.tm_hour==-1) {
                  cout << "Adjusting2." << endl;
                  
                  t_start+=3600;
                  t_end+=3600;
                  
                  dt_start=difftime(sun_mid,t_start)/(24*3600);
                  dt_end=difftime(sun_mid,t_end)/(24*3600);
                  
                  tm_start2=*localtime(&t_start);
                  tm_end2=*localtime(&t_end);
                  
                }
                
                // Check that this date is not excluded by an
                // "EXDATE:" entry
                
                bool excluded=false;
                for (field_it it=c.events[i].begin();
                     it!=c.events[i].end();it++) {
                  
                  if (it->first==((string)"EXDATE")) {
                    bool all_day;
                    tm_t tm_exclude=str_to_date(it->second,c.cal_tz,
                                                c.gmt_offset,all_day);
                    if (tm_start2.tm_mday==tm_exclude.tm_mday &&
                        tm_start2.tm_mon==tm_exclude.tm_mon &&
                        tm_start2.tm_year==tm_exclude.tm_year) {
                      if (verbose>1) {
                        cout << "  Excluding "
                             << c.events[i].find("SUMMARY")->second
                             << " from the entry EXDATE "
                             << it->second << endl;
                      }
                      excluded=true;
                    }
                  }
                }
                
                if (excluded==false && dt_start<0.0 && dt_start>-7.0) {
                  if (verbose>1) {
                    cout << "  Event happening next week by rule." << endl;
                    cout << "  Start time: " << tm_start2.tm_hour << " "
                         << tm_start2.tm_min << endl;
                  }
                  if (c.events[i].find("SUMMARY")!=c.events[i].end() &&
                      verbose>1) {
                    cout << "  Summary: "
                         << c.events[i].find("SUMMARY")->second << endl;
                  }

                  // Create a new event with an adjusted DTSTART and
                  // DTEND time
                  multimap<string,string,less<string>> event2=c.events[i];
                  event2.find("DTSTART")->second=
                    ((string)"TZID=America/New_York:")+
                    date_to_str(tm_start2);
                  event2.find("DTEND")->second=
                    ((string)"TZID=America/New_York:")+
                    date_to_str(tm_end2);

                  cout << "  Setting tm_start,tm_end to "
                       << asctime_cpp(tm_start2) << " "
                       << asctime_cpp(tm_end2) << endl;

                  int ix=-dt_start;
                  if (ix<0 || ix>6) {
                    O2SCL_ERR("Ix fail 2.",1);
                  }
                  if (verbose>1) {
                    cout << "  Inserting on " << weekdays[ix] << "." << endl;
                    insert_ordered(events_nw[ix],event2);
                  }
                }
                if (t_start>t_until) {
                  if (verbose>1) {
                    cout << "  Stopping because passed until date." << endl;
                  }
                  done=true;
                }
                if (t_start>sun_mid+7*24*3600) {
                  if (verbose>1) {
                    cout << "  Stopping because start date is more than one "
                         << "week past Sunday" << endl;
                  }
                  done=true;
                }
                if (verbose>2) {
                  char ch;
                  cin >> ch;
                }
              }
            }
          }
        }
      }

      if (verbose>0) {
        for(size_t j=0;j<7;j++) {
          cout << "Events happening next " << weekdays[j]
               << ": " << events_nw[j].events.size()
               << endl;
        }
      }

      cout << "Making html." << endl;
      
      ofstream fout;
      if (week_offset==0) {
        fout.open("ics.html");
      } else {
        fout.open(((string)"ics")+o2scl::itos(week_offset)+".html");
      }

      ifstream fin;
      fin.open("ics_header.html");
      std::string sfile;
      while (getline(fin,sfile)) {
        fout << sfile << endl;
      }
      fin.close();

      if (tnow.tm_isdst==1) {
        fout << "<p>All times EDT.</p>" << endl;
      } else {
        fout << "<p>All times EST.</p>" << endl;
      }
      fout << endl;

      // Table header
      fout << "<table class=\"table table-sm table-responsive\"><thead><tr>"
           << endl;

      // Row label column header
      fout << "<td width=\"70px\"></td>" << endl;

      // Output the dates, skipping the weekend days
      for(size_t j=1;j<6;j++) {
        time_t t_day=sun_mid+((double)j)*24*3600;
        tm_t tm_day=*localtime(&t_day);
        if (j%2==1) {
          fout << "<td class=\"text-center\" width=\"250px\">" << weekdays[j]
               << "<br>" << tm_day.tm_mday
               << "</td>" << endl;
        } else {
          fout << "<td class=\"table-secondary text-center\" "
               << "width=\"250px\">" << weekdays[j]
               << "<br>" << tm_day.tm_mday
               << "</td>" << endl;
        }
      }

      fout << "</tr></thead><tbody><tr>" << endl;

      if (found_an_all_day) {
        fout << "<td>all-day</td>" << endl;
        
        // Output the events, skipping the weekend days
        for(size_t j=1;j<6;j++) {
          time_t t_day=sun_mid+((double)j)*24*3600;
          tm_t tm_day=*localtime(&t_day);
          if (j%2==0) {
            fout << "<td>" << endl;
          } else {
            fout << "<td class=\"table-secondary\">" << endl;
          }

          for(size_t k=0;k<events_nw[j].events.size();k++) {
            
            string start2=events_nw[j].events[k].find("DTSTART")->second;
            string end2=events_nw[j].events[k].find("DTEND")->second;
            bool all_day;
            tm_t tm_start2=str_to_date(start2,c.cal_tz,c.gmt_offset,all_day);
            time_t t_start2=mktime(&tm_start2);
            tm_t tm_end2=str_to_date(end2,c.cal_tz,c.gmt_offset,all_day);
            time_t t_end2=mktime(&tm_end2);
            
            if (all_day==true) {
              
              fout << "<div style=\"border-radius: 7px; "
                   << "overflow: hidden; font-size: 80%; "
                   << "border: 1px solid #aad; background-color: #cee;\">"
                   << events_nw[j].events[k].find("SUMMARY")->second
                   << "</div>" << endl;
              
            }
          }
          fout << "</td>" << endl;
          
        }

        fout << "</tr><tr>" << endl;
      }

      // Cell containing row times
      fout << "<td>" << endl;
      fout << "<div style=\"height: 100px; position:" << endl;
      fout << "relative;\">8am&nbsp;EDT</div>" << endl;
      fout << "<div style=\"height: 100px; position:" << endl;
      fout << "relative;\">10am&nbsp;EDT</div>" << endl;
      fout << "<div style=\"height: 100px; position:" << endl;
      fout << "relative;\">noon&nbsp;EDT</div>" << endl;
      fout << "<div style=\"height: 100px; position:" << endl;
      fout << "relative;\">2pm&nbsp;EDT</div>" << endl;
      fout << "<div style=\"height: 100px; position:" << endl;
      fout << "relative;\">4pm&nbsp;EDT</div>" << endl;
      fout << "</td>" << endl;
      
      // Output the events, skipping the weekend days
      for(size_t j=1;j<6;j++) {
        time_t t_day=sun_mid+((double)j)*24*3600;
        tm_t tm_day=*localtime(&t_day);
        if (j%2==0) {
          fout << "<td>" << endl;
        } else {
          fout << "<td class=\"table-secondary\">" << endl;
        }
        fout << "<div style=\"position: relative; display: "
             << "inline-block: height: 600px;\">" << endl;
        
        // Create a vector which describes the horizontal position and
        // the horizontal size. By default, hpos is 0 (to indicate the
        // left-most event) and hsize is 1 (to indicate 1 simultaneous
        // event per day)
        vector<int> hpos(events_nw[j].events.size());
        vector<int> hsize(events_nw[j].events.size());
        for(size_t k=0;k<events_nw[j].events.size();k++) {
          hpos[k]=0;
          hsize[k]=1;
        }
          
        for(size_t k=0;k<events_nw[j].events.size();k++) {
          
          string start2=events_nw[j].events[k].find("DTSTART")->second;
          string end2=events_nw[j].events[k].find("DTEND")->second;
          bool all_day;
          tm_t tm_start2=str_to_date(start2,c.cal_tz,c.gmt_offset,all_day);
          time_t t_start2=mktime(&tm_start2);
          tm_t tm_end2=str_to_date(end2,c.cal_tz,c.gmt_offset,all_day);
          time_t t_end2=mktime(&tm_end2);

          if (all_day==false) {

            // If this event overlaps with the next event
            if (k<events_nw[j].events.size()-1 &&
                events_overlap(c,events_nw[j].events[k],
                               events_nw[j].events[k+1])==true) {
              // Increase the width of the kth event, ensure
              // the (k+1)th event has the same width, and push
              // the (k+1)th event to the right
              hsize[k]++;
              hsize[k+1]=hsize[k];
              hpos[k+1]=hsize[k]-1;
            }
          
            double hours_after_eight=(tm_start2.tm_hour-8)+
              tm_start2.tm_min/60.0;
            if (hours_after_eight<0) hours_after_eight=0;
            double length_in_hours=((double)difftime(t_end2,t_start2))/
              3600.0;

            double width=100.0/((double)hsize[k]);
            double left=hpos[k]*width;
            
            fout << "<div style=\"position: absolute; left: "
                 << ((int)left) << "px; top: "
                 << ((int)(hours_after_eight*50)) << "px; "
                 << "height: " << ((int)(length_in_hours*50)) << "px; "
                 << "z-index: 2; width: "
                 << ((int)width) << "%; border-radius: 7px; "
                 << "overflow: hidden; font-size: 80%; background-color:"
                 << "#cee; border: 1px solid #aad;\">" << endl;

            // Reformat starting end ending time into a shorter
            // format
            int hour_start=tm_start2.tm_hour;
            if (hour_start>12) hour_start-=12;
            if (tm_start2.tm_min==0) {
              fout << hour_start;
            } else {
              fout << hour_start << ':';
              if (tm_start2.tm_min<10) {
                fout << "0" << tm_start2.tm_min;
              } else {
                fout << tm_start2.tm_min;
              }
            }
            fout << '-';
            int hour_end=tm_end2.tm_hour;
            if (hour_end>12) {
              fout << hour_end-12;
            } else {
              fout << hour_end;
            }
            if (tm_end2.tm_min!=0) {
              fout << ':';
              if (tm_end2.tm_min<10) {
                fout << "0" << tm_end2.tm_min;
              } else {
                fout << tm_end2.tm_min;
              }
            }
            if (hour_end>11) {
              fout << 'p';
            } else {
              fout << 'a';
            }
            
            fout << ": " << events_nw[j].events[k].find("SUMMARY")->second
                 << endl;
            
            fout << "</div>" << endl;

          }
          
        }
        fout << "</div>" << endl;
      }
    
      // Table footer
      fout << "</tr></tbody></table>" << endl;

      fin.open("ics_footer.html");
      while (getline(fin,sfile)) {
        fout << sfile << endl;
      }
      fin.close();
      
      return 0;
    }
  
  };

}

using namespace o2scl_ics;

int main(int argv, char *argc[]) {

  int week=0;
  if (argv>=2) {
    week=o2scl::stoi(argc[1]);
    if (week>2) {
      cerr << "Invalid week: " << argc[1] << endl;
      exit(-1);
    }
    cout << "Using week: " << week << endl;
  }
  
  calendar c;
  ics_parser ip;
  ip.parse("/Users/awsteiner/wcs/int8/schedule.ics",c);
  if (false) {
    ofstream fout;
    fout.open("test.ics");
    c.output(fout);
    fout.close();
  }
  vector<calendar> next_week;
  ip.make_week(c,next_week,week);
  return 0;
}

